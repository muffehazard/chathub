package main

import (
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
)

const channelCleanupInterval = time.Minute

type Connection struct {
	channel string
	conn    *websocket.Conn
}

type Hub struct {
	opts     Opts
	upgrader websocket.Upgrader
	channels map[string]*Channel
	connChan chan Connection
	killChan chan struct{}
}

func NewHub(opts Opts) *Hub {
	hub := &Hub{
		opts: opts,
		upgrader: websocket.Upgrader{
			ReadBufferSize:    1024,
			WriteBufferSize:   1024,
			EnableCompression: true,
		},
		channels: make(map[string]*Channel),
		connChan: make(chan Connection),
		killChan: make(chan struct{}),
	}

	go hub.handle()

	hub.logMsg("New hub with opts: %+v", hub.opts)

	return hub
}

func (h Hub) logMsg(format string, v ...interface{}) {
	log.Printf("[Hub] "+format, v...)
}

func (h *Hub) WebsocketHandler(w http.ResponseWriter, r *http.Request) {
	conn, err := h.upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Printf("Upgrade: %v", err)
		return
	}

	conn.SetReadLimit(h.opts.msgSizeLimit)

	channel := mux.Vars(r)["channel"]

	h.logMsg("Connection from %v to channel '%v'", conn.RemoteAddr(), channel)

	select {
	case h.connChan <- Connection{
		channel: channel,
		conn:    conn,
	}:
	case <-h.killChan:
		conn.Close()
	}
}

func (h *Hub) handle() {
	cleanTicker := time.NewTicker(channelCleanupInterval)
	defer cleanTicker.Stop()
	defer h.logMsg("Shutdown")

	for {
		select {
		case conn := <-h.connChan:
			c, ok := h.channels[conn.channel]
			if !ok {
				c = NewChannel(conn.channel, h.opts)
				h.channels[conn.channel] = c
				h.logMsg("Channel %v created", conn.channel)
			}

			c.connChan <- conn.conn
		case <-cleanTicker.C:
			for name, channel := range h.channels {
				select {
				case active := <-channel.activeChan:
					if !active {
						close(channel.connChan)
						delete(h.channels, name)
						h.logMsg("Channel %v deleted", name)
					}
				default:
				}
			}
		case <-h.killChan:
			for name, channel := range h.channels {
				close(channel.connChan)
				delete(h.channels, name)
			}

			return
		}
	}
}
