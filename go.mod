module gitlab.com/muffehazard/chathub

go 1.12

require (
	github.com/gorilla/mux v1.7.1
	github.com/gorilla/websocket v1.4.0
	golang.org/x/time v0.0.0-20190308202827-9d24e82272b4
)
