package main

import (
	"fmt"
	"log"
	"time"

	"github.com/gorilla/websocket"
	"golang.org/x/time/rate"
)

const (
	pongWait   = time.Minute
	pingPeriod = (pongWait * 9) / 10
)

type Client struct {
	limiter   *rate.Limiter
	currBurst int
	channel   *Channel
	conn      *websocket.Conn
	sendChan  chan []byte
	killChan  chan struct{}
}

func NewClient(channel *Channel, conn *websocket.Conn) *Client {
	client := &Client{
		limiter:  rate.NewLimiter(rate.Every(channel.opts.clientRateLimit), channel.opts.clientBurstLimit),
		channel:  channel,
		conn:     conn,
		sendChan: make(chan []byte, channel.opts.channelBurstLimit),
		killChan: make(chan struct{}),
	}

	go client.handle()

	return client
}

func (c Client) String() string {
	return fmt.Sprintf("%v", c.conn.RemoteAddr())
}

func (c Client) logMsg(format string, v ...interface{}) {
	log.Printf(fmt.Sprintf("[Client %v] ", c)+format, v...)
}

func (c *Client) handle() {
	defer close(c.killChan)
	defer c.logMsg("Disconnect")

	go c.writeMsg()

	c.conn.SetReadDeadline(time.Now().Add(pongWait))
	c.conn.SetPongHandler(func(string) error {
		c.conn.SetReadDeadline(time.Now().Add(pongWait))
		return nil
	})

	for {
		r := c.limiter.Reserve()
		if !r.OK() {
			return
		}

		if r.Delay() > 0 {
			c.currBurst++
			if c.currBurst > c.channel.opts.clientSpamLimit {
				c.logMsg("Spam cutoff")
				return
			}

			time.Sleep(r.Delay())
		} else {
			c.currBurst = 0
		}

		_, message, err := c.conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseNormalClosure, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				c.logMsg("Unexpected read: %v", err)
			}

			return
		}

		c.channel.msgChan <- message
	}
}

func (c *Client) writeMsg() {
	defer c.conn.Close()
	ticker := time.NewTicker(pingPeriod)
	defer ticker.Stop()

	for {
		select {
		case msg, ok := <-c.sendChan:
			if !ok {
				return
			}

			c.conn.SetWriteDeadline(time.Now().Add(c.channel.opts.clientWriteWait))

			w, err := c.conn.NextWriter(websocket.BinaryMessage)
			if err != nil {
				c.logMsg("Writer: %v", err)
				return
			}

			_, err = w.Write(msg)
			if err != nil {
				c.logMsg("Write: %v", err)
				return
			}

			err = w.Close()
			if err != nil {
				c.logMsg("Close: %v", err)
				return
			}

		case <-ticker.C:
			c.conn.SetWriteDeadline(time.Now().Add(c.channel.opts.clientWriteWait))
			if err := c.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				c.logMsg("Ping: %v", err)
				return
			}

		case <-c.killChan:
			return
		}
	}

}
