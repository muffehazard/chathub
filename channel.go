package main

import (
	"fmt"
	"log"
	"time"

	"golang.org/x/time/rate"

	"github.com/gorilla/websocket"
)

const clientCleanupInterval = time.Minute

type Channel struct {
	name       string
	opts       Opts
	limiter    *rate.Limiter
	clients    map[*Client]struct{}
	connChan   chan *websocket.Conn
	msgChan    chan []byte
	activeChan chan bool
}

func NewChannel(name string, opts Opts) *Channel {
	channel := &Channel{
		name:       name,
		opts:       opts,
		limiter:    rate.NewLimiter(rate.Every(opts.channelRateLimit), opts.channelBurstLimit),
		clients:    make(map[*Client]struct{}),
		connChan:   make(chan *websocket.Conn),
		msgChan:    make(chan []byte),
		activeChan: make(chan bool),
	}

	go channel.handle()

	return channel
}

func (c Channel) String() string {
	return fmt.Sprintf("%v", c.name)
}

func (c Channel) logMsg(format string, v ...interface{}) {
	log.Printf(fmt.Sprintf("[Channel %v] ", c)+format, v...)
}

func (c *Channel) handle() {
	cleanTicker := time.NewTicker(clientCleanupInterval)
	defer cleanTicker.Stop()
	defer c.logMsg("Shutdown")
	defer func() {
		for client := range c.clients {
			close(client.sendChan)
			delete(c.clients, client)
		}
	}()

	for {
		select {
		case conn, ok := <-c.connChan:
			if !ok {
				return
			}

			client := NewClient(c, conn)
			c.clients[client] = struct{}{}
			c.logMsg("New client: %v", client)
		case msg := <-c.msgChan:
			r := c.limiter.Reserve()
			if !r.OK() {
				return
			}

			if r.Delay() > 0 {
				c.logMsg("Throttling for %v", r.Delay())
				time.Sleep(r.Delay())
			}

			for client := range c.clients {
				select {
				case client.sendChan <- msg:
				case <-client.killChan:
					delete(c.clients, client)
					c.logMsg("Send: Client left: %v", client)
				default:
					close(client.sendChan)
					delete(c.clients, client)
				}
			}
		case <-cleanTicker.C:
			for client := range c.clients {
				select {
				case <-client.killChan:
					delete(c.clients, client)
					c.logMsg("Clean: Client left: %v", client)
				default:
				}
			}
		case c.activeChan <- len(c.clients) > 0:
		}
	}
}
