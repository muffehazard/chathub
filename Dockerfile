FROM golang:latest as build-env
WORKDIR /build
COPY . .
RUN go get -u

FROM alpine:latest
RUN apk add --no-cache \
    openssl \
    ca-certificates \
    libc6-compat
COPY --from=build-env /go/bin/chathub /usr/local/bin/chathub
EXPOSE 80
ENTRYPOINT ["chathub"]
