package main

import (
	"context"
	"flag"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/gorilla/mux"
)

type Opts struct {
	channelRateLimit  time.Duration
	channelBurstLimit int
	clientRateLimit   time.Duration
	clientBurstLimit  int
	clientSpamLimit   int
	clientWriteWait   time.Duration
	msgSizeLimit      int64
}

func main() {
	interruptChan := make(chan os.Signal, 1)
	signal.Notify(interruptChan, os.Interrupt)

	channelRateLimit := flag.Duration("channel_rate_limit", 500*time.Millisecond, "Channel rate limit")
	channelBurstLimit := flag.Int("channel_burst_limit", 10, "Channel burst limit")
	clientRateLimit := flag.Duration("client_rate_limit", time.Second, "Client rate limit")
	clientBurstLimit := flag.Int("client_burst_limit", 5, "Client burst limit")
	clientSpamLimit := flag.Int("client_spam_limit", 5, "Client spam limit")
	clientWriteWait := flag.Duration("client_write_wait", 10*time.Second, "Client write wait")
	msgSizeLimit := flag.Int64("msg_size_limit", 10*1000*1000, "Maximum message size")

	flag.Parse()

	opts := Opts{
		channelBurstLimit: *channelBurstLimit,
		channelRateLimit:  *channelRateLimit,
		clientRateLimit:   *clientRateLimit,
		clientBurstLimit:  *clientBurstLimit,
		clientSpamLimit:   *clientSpamLimit,
		clientWriteWait:   *clientWriteWait,
		msgSizeLimit:      *msgSizeLimit,
	}

	hub := NewHub(opts)
	defer close(hub.killChan)

	mx := mux.NewRouter()
	mx.HandleFunc("/", hub.WebsocketHandler)
	mx.HandleFunc("/{channel}", hub.WebsocketHandler)

	server := http.Server{
		Addr:    ":80",
		Handler: mx,
	}

	doneChan := make(chan struct{})
	go func() {
		select {
		case <-interruptChan:
			ctx, cancel := context.WithDeadline(context.Background(), time.Now().Add(time.Second*10))
			defer cancel()

			server.Shutdown(ctx)
		case <-doneChan:
		}
	}()

	go func() {
		defer close(doneChan)

		log.Printf("Listening on %v", server.Addr)
		err := server.ListenAndServe()
		if err != http.ErrServerClosed {
			log.Printf("ListenAndServe: %v", err)
		}
	}()

	<-doneChan
}
